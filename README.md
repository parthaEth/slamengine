# README #

1. do "git clone https://parthaEth@bitbucket.org/parthaEth/slamengine.git"
2. you need "Lightweight Filtering" from 'Michael Bloesch' so do 
      * "git clone https://bitbucket.org/bloesch/lightweight_filtering.git"
3. You also will need 'kindr' from 'ethz-asl' so do
      * "git clone git@github.com:ethz-asl/kindr.git"
4. Everything in 'src' folder of a catkin workspace, because every thing is ROS dependent. If you do not have a catkin workspace, create one by following [this tutorial](http://wiki.ros.org/ROS/Tutorials).
5. Finally run "catkin build" in the catkin workspace. Normally named as "catkin_ws".
6. when the build finishes. from 3 different terminal do the following
       1. roslaunch rcars_estimator vi-sensor_left_bag.launch 
       2. roslaunch rcars_visualization visualization.launch
       3. rosbag play <bagfilename_with_path>.bag
7. This will run the code for [recorded data found in here](https://bitbucket.org/adrlab/rcars/downloads).
8. Do not forget to source the "~/catkin_ws/devel/setup.bash" file every time you open a new terminal otherwise the system will not find the newly built packages